README.md

# What is this?

This is a PUBLIC example of how to configure a dual Ubuntu server cluster (on a Proxmox cluster) for ISC DHCP including NetBoot.XYZ PXEboot support for legacy BIOS and UEFI BIOS.

# Commentary

I used to have a roughly decade old private repo with hundreds of commits, mostly for hardware I no longer own LOL.  I did not feel like examining hundreds of commits to make sure nothing bad would be made public, so I just wiped the old config, which is why this repo history starts in late July 2022.  

This repo makes the most sense in the context of being "downstream" of the following repo:

https://gitlab.com/SpringCitySolutionsLLC/ansible

# Initial Setup of a new server

Log into dhcp01 or dhcp02 as root
```
cd /root 
ssh-keygen
Add the root ssh key /root/.ssh/id_rsa.pub to gitlab as a new SSH key
apt-get install isc-dhcp-server
service isc-dhcp-server stop
mv /etc/dhcp/dhcpd.conf /etc/dhcp/dhcpd.conf.old
git clone git@gitlab.com:SpringCitySolutionsLLC/dhcp.git dhcp
cd /root/dhcp
./restart.sh
```

At this point you decide to run ./on.sh or ./off.sh depending on situation.

#
